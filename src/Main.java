import java.util.Random;

import processing.core.PApplet;

public class Main extends PApplet {
	
	static String[] texts = {"antony", "hamlet", "julius", "kinghenryv", "kinglear", "macbeth", "midsummer", "othello", "romeo", "sonnets", "tamingoftheshrew"};
	static String text = texts[random(texts.length)];
	static TextGenerator t = new TextGenerator("texts/"+text+".txt");
	
	public static void main(String[] args) {
		main("Main");
	}
	
	public void settings() {
		size(1900, 420);
	}
	
	public void setup() {
		textSize(24);
		drawSentences(10);
	}
	
	public void drawSentences(int n) {
		background(0);
		text(text + ".txt", 15, 35);
		for(int i = 0; i < n; i++) {
			text(t.generateSentence(), 15, 35 * (i+1) + 35);
		}
	}
	
	public void draw() {}
	
	public void mouseClicked() {
		reset();
		drawSentences(10);
	}
	
	/**
	 * Choose new text and assign it to the TextGenerator
	 */
	public void reset() {
		text = texts[random(texts.length)];
		t.setText(text);
	}
	
	/**
	 * @param bound - int
	 * @return Random integer from 0 to <b>bound</b>
	 */
	private static int random(int bound) {
		return new Random().nextInt(bound);
	}
}